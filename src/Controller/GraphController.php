<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_', methods: ['GET'])]
class GraphController extends AbstractController
{
    #[Route('/stats', name: 'stats', methods: ['GET'])]
    public function index(OrderRepository $orderRepository, UserRepository $userRepository): Response
    {
        $orders = $orderRepository->findAll();
        $chiffreAffaire = 0;

        foreach ($orders as $order){
            foreach ($order->getOrderDetails() as $detail){
                $chiffreAffaire = $chiffreAffaire + ($detail->getQuantity() * $detail->getPrice());
               // $chiffreAffaire = $chiffreAffaire + $detail->getQuantity() * $detail->getPrice();
            }
        }

        $json = [
            "nbCommandes"=> $orderRepository->count([]),
            'nombreNouveauxClients'=> $userRepository->getNouveauClient()["newUser"],
            'chiffreAffaire'=> round($chiffreAffaire,2)
        ];

        return new JsonResponse($json);
    }

    #[Route('/graph-1', name: 'graph-1', methods: ['GET'])]
    public function graph1(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();

        $categoryLabel = [];
        $categoryData = [];

        foreach ($categories as $category){
            $categoryLabel[] = $category->getName();
            $categoryData[] = count($category->getProducts());
        }

        return new JsonResponse([
            'label'=> $categoryLabel,
            'data'=> $categoryData
        ]);
    }

    #[Route('/graph-2', name: 'graph-2', methods: ['GET'])]
    public function graph2(UserRepository $userRepository){
        $clients = $userRepository->count([]);

        $dataLabel = ['Ancien client', 'Nouveau client'];

        $dataData = [$clients - $userRepository->getNouveauClient()["newUser"],
            $userRepository->getNouveauClient()["newUser"]];

        return new JsonResponse([
            'label'=> $dataLabel,
            'data'=> $dataData
        ]);


    }



}
