<?php

namespace App\Repository;

use App\Entity\Cart;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cart|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cart|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cart[]    findAll()
 * @method Cart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cart::class);
    }

    public function save(Cart $cart){
        if($cart->getQuantity() <= 0){
            //$this->getEntityManager()->remove($cart);
            $query = $this->getEntityManager()->createQuery('DELETE FROM App\Entity\Cart c WHERE c.product = '.$cart->getProduct()->getId().' AND c.user = '.$cart->getUser()->getId());
            $result = $query->getResult();
        } else {
            $this->getEntityManager()->merge($cart);
            $this->getEntityManager()->flush();
        }
    }

    //public function getByUser(User $user){

        // test 1 : findBy : pas de mapping au niveau du produit
        //return $this->findBy(['user' => $user]);

        // test 2 : createQueryBuilder : pas de mapping au niveau du produit
        // return $this->createQueryBuilder('c')
        // ->where('c.user = :user')
        // ->setParameter('user', $user)
        // ->getQuery()
        // ->getResult();

        // test 3 : avec query dql
        // $query = $this->getEntityManager()->createQuery('SELECT c, p FROM App\Entity\Cart c 
        // LEFT JOIN App\Entity\Product p
        // WITH p.id = c.product
        // WHERE c.user = '.$user->getId());
        // return $query->getResult();

        // tests
        //dd($this->findAll());

    //}

    /**
     * Remove the cart 
     */
    public function remove(User $user){
        $query = $this->getEntityManager()->createQuery('DELETE FROM App\Entity\Cart c WHERE c.user = '.$user->getId());
        $result = $query->getResult();
    }

    /*public function save(Cart $cart){
        $qb = $this->em->createQueryBuilder();
        $query = $qb->update('Cart', 'c')
        ->set('c.quantity', ':quantity')
        ->where('c.product = :product')
        ->andWhere('c.user = :user')
        ->setParameter('quantity', $cart->getQuantity())
        ->setParameter('product', $cart->getProduct())
        ->setParameter('user', $cart->getUser());

        $result = $query->execute();

    }*/

    // /**
    //  * @return Cart[] Returns an array of Cart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cart
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
