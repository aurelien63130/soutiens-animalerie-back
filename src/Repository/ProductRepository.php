<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\CategoryRepository;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    private $categoryRepository;

    public function __construct(ManagerRegistry $registry , CategoryRepository $categoryRepository)
    {
        parent::__construct($registry, Product::class);
        $this->categoryRepository = $categoryRepository;
    }

    public function findByKeywords($keywords)
    {
        $qb =  $this->createQueryBuilder('p')
            ->leftJoin('p.category', 'c')
            ->where('p.isActive = 1')
            ->andWhere('p.quantityStock > 0')
            ->andWhere('p.name LIKE :keywords')
            ->orWhere('p.description LIKE :keywords')
            ->orWhere('c.name LIKE :keywords')
            ->setParameter('keywords', '%'.$keywords.'%');
        return $qb->getQuery()->getResult();
    }

    public function findByCategory($idCategory)
    {
        $qb =  $this->createQueryBuilder('p')
            ->leftJoin('p.category', 'c')
            ->where('p.isActive = 1')
            ->andWhere('p.quantityStock > 0')
            ->andWhere('c.id = :idCategory')
            ->setParameter('idCategory', $idCategory);
        return $qb->getQuery()->getResult();
    }

     public function findByCategoryName($sNameCategory)
    {
        $qb =  $this->createQueryBuilder('p')
            ->leftJoin('p.category', 'c')
            ->where('p.isActive = 1')
            ->andWhere('p.quantityStock > 0')
            ->andWhere('c.name = :name')
            ->setParameter('name', $sNameCategory);
        return $qb->getQuery()->getResult();
    }

    public function uploadPhoto(){
        
    }

    public function findByFilters($filters, $page = 1, $nbResult = 12){
        $qb =  $this->createQueryBuilder('p')->where('p.isActive = 1')->andWhere('p.quantityStock > 0');

        if(isset($filters['category'])){
            $qb->leftJoin('p.category', 'c')
            ->andWhere('c.name = :name')
            ->setParameter('name', $filters['category']);
        } else if(isset($filters['parent'])){

            $parent = $this->categoryRepository->findOneBy(['name' => $filters['parent']]);

            $qb->leftJoin('p.category', 'c')
            ->andWhere('c.parent = :id')
            ->setParameter('id', $parent);
        }

        if(isset($filters['brand'])){
            $brand = implode(',', $filters['brand']);
            $qb->andWhere('p.brand IN (:brand)')->setParameter('brand', $brand);
        }

        if(isset($filters['keywords'])){
            $qb->andWhere('p.name LIKE :keywords OR p.description LIKE :keywords')
            ->setParameter('keywords', '%'.$filters['keywords'].'%');
        }

        // prix max 
        $qb->select('MAX(p.price)');
        $maxPrice = ceil($qb->getQuery()->getSingleScalarResult());

        if(isset($filters['priceMin'])){
            $qb->andWhere('p.price > :valMin')->setParameter('valMin', $filters['priceMin']);
        }

        if(isset($filters['priceMax'])){
            $qb->andWhere('p.price < :valMax')->setParameter('valMax', $filters['priceMax']);
        }

        // nombre de résultats
        $qb->select('count(p)');
        $nbTotal = $qb->getQuery()->getSingleScalarResult();

        // résultats et nombre de pages
        if ($nbTotal>0) {
            // order by
            if(isset($filters['order_by'])){
                if($filters['order_by'] == 'price_asc'){
                    $qb->orderBy('p.price', 'ASC');
                } elseif($filters['order_by'] == 'price_desc'){
                    $qb->orderBy('p.price', 'DESC');
                } elseif($filters['order_by'] == 'brand'){
                    $qb->orderBy('p.brand', 'ASC');
                } elseif($filters['order_by'] == 'name'){
                    $qb->orderBy('p.name', 'ASC');
                }
            }
            $qb->select('p');
            $qb->setMaxResults($nbResult)->setFirstResult(($page*$nbResult)-$nbResult);
            $products = $qb->getQuery()->getResult();
            $nbPages = ceil($nbTotal/$nbResult);
        } else {
            $products = [];
            $nbPages = 1;
        }

        //dd($products);

        return ['total' => $nbTotal, 'products' => $products, 'pages' => $nbPages, 'maxPrice' => $maxPrice];
    }

    public function findProduct($id = null, $name = null, $reference = null, $quantityStockMin = null, $quantityStockMax = null, $priceMin = null, $priceMax = null, $isActive = null, $isBest = null, $brand = null, $category = null  ){
        $qb = $this->createQueryBuilder('p');

        if($id){
            $qb->andWhere('p.id = :id')->setParameter('id', $id);
        }

        if($name){
            $qb->andWhere('p.name LIKE :name')->setParameter('name', '%'.$name.'%');
        }

        if($reference){
            $qb->andWhere('p.reference LIKE :reference')->setParameter('reference', '%'.$reference.'%');
        }

        if($quantityStockMin){
            $qb->andWhere('p.quantityStock >= :qtyStockMin')->setParameter('qtyStockMin', $quantityStockMin);
        }

        if($quantityStockMax){
            $qb->andWhere('p.quantityStock <= :qtyStockMax')->setParameter('qtyStockMax', $quantityStockMax);
        }

        if($priceMin){
            $qb->andWhere('p.price >= :priceMin')->setParameter('priceMin', $priceMin);
        }

        if($priceMax){
            $qb->andWhere('p.price <= :priceMax')->setParameter('priceMax', $priceMax);
        }

        if($isActive){
            $qb->andWhere('p.isActive = :isActive')->setParameter('isActive', $isActive);
        }

        if($isBest){
            $qb->andWhere('p.isBest = :isBest')->setParameter('isBest', $isBest);
        }

        if($brand){
            $qb->andWhere('p.brand = :brand')->setParameter('brand', $brand);
        }

        if($brand){
            $qb->andWhere('p.brand = :brand')->setParameter('brand', $brand);
        }

        if($category){
            $qb->andWhere('p.category = :category')->setParameter('category', $category);
        }

        return $qb->getQuery()->getResult();
    }

    /*public function getMostSelled($limit = 10){
        $oQuery = $this->_em->createQuery("select MAX(o.product level1");
    }*/

    /*public function findAll(){
        return $this->createQueryBuilder('p')
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }*/

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
