<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }



    public function getAllCategories(){
        $oQuery = $this->_em->createQuery("select categ0.name level1,
            categ1.name as level2,
            categ2.name as level3,
            categ3.name as level4
            from App\Entity\Category categ0
            left outer join App\Entity\Category categ1 with categ1.parent = categ0.id and categ1.isActive = '1'
            left outer join App\Entity\Category categ2 with categ2.parent = categ1.id and categ2.isActive = '1'
            left outer join App\Entity\Category categ3 with categ3.parent = categ2.id and categ3.isActive = '1'
            where categ0.parent is null and categ0.isActive = '1'
            order by categ0.priority desc, categ1.priority desc, categ2.priority desc, categ3.priority desc, level2, level3, level4");
        $aResults = $oQuery->getResult();
        $aReturn = [];
        foreach($aResults as $aResult){
            if(isset($aResult['level4'])){
                $aReturn[$aResult['level1']][$aResult['level2']][$aResult['level3']][$aResult['level4']] = null;
            } else if(isset($aResult['level3'])){
                $aReturn[$aResult['level1']][$aResult['level2']][$aResult['level3']] = null;
            } else if(isset($aResult['level2'])){
                $aReturn[$aResult['level1']][$aResult['level2']] = null;
            } else{
                $aReturn[$aResult['level1']] = null;
            }
        }
        return $aReturn;
    }

    /*public function getParent($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }*/

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
