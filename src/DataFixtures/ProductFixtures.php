<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\BrandFixtures;
use App\DataFixtures\CategoryFixtures;
use App\Entity\Photo;
use Faker;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        $catMax = 24;

        $aParentCategory[8] = "Chien";
        $aParentCategory[9] = "Chien";
        $aParentCategory[10] = "Chien";
        $aParentCategory[11] = "Chien";
        $aParentCategory[12] = "Chat";
        $aParentCategory[13] = "Chat";
        $aParentCategory[14] = "Chat";
        $aParentCategory[15] = "Chat";
        $aParentCategory[16] = "Poules";
        $aParentCategory[17] = "Poules";
        $aParentCategory[18] = "Poules";
        $aParentCategory[19] = "Oiseaux";
        $aParentCategory[20] = "Oiseaux";
        $aParentCategory[21] = "Rongeurs";
        $aParentCategory[22] = "Rongeurs";
        $aParentCategory[23] = "Poissons";
        $aParentCategory[24] = "Reptiles";

        // Chiens
        $aKeywords[8] = ['Croquettes pour chiens', 'Friandises pour chien', 'Pâtée pour chien', 'Sachet de', 'Boîte de', 'Nourriture', 'Aliment'];
        $aKeywords[9] = ['Jouet pour chien', 'Accessoire pour chien', 'Super truc pour chien', 'Anti-stress', 'Pull pour chien', 'Collier de dressage', 'Muselière'];
        $aKeywords[10] = ['Médicament pour chien', 'Produit anti-puces pour chien', 'Pommade pour chien', 'Hygiène bucco-dentaire'];
        $aKeywords[11] = ['Nettoyant pour chien', 'Kit dentaire', 'Shampooing pour chien', 'Soin des oreilles'];
        // Chats
        $aKeywords[12] = ['Croquettes pour chat', 'Friandises pour chat', 'Pâtée pour chat', 'Nourriture pour chat', 'Aliment pour chat', 'Herbe à chat'];
        $aKeywords[13] = ['Jouet', 'Accessoire', 'Super', 'Coupe-griffes'];
        $aKeywords[14] = ['Médicament', 'Produit anti-puces', 'Hygiène bucco-dentaire'];
        $aKeywords[15] = ['Nettoyant', 'Shampooing', 'Litière végétale'];
        // Poules
        $aKeywords[16] = ['Mélange complet poules pondeuses', 'Mélange Basse-cour'];
        $aKeywords[17] = ['Ecuelle', 'Mangeoire', 'Super'];
        $aKeywords[18] = ['Coquilles d\'huîtres pour Poules Pondeuses', 'Pot Spécial Ponte Aliment minéral poules pondeuses', 'Vermifuge', 'Antipoux pour poules'];
        // Oiseaux
        $aKeywords[19] = ['Mélange Perroquet Tropical', 'Graines de Tournesol', 'Graines Alpiste', 'Alimentation oiseaux', 'Boîte de', 'Nourriture', 'Aliment'];
        $aKeywords[20] = ['Jouet', 'Abreuvoir', 'Perchoir pour perroquets', 'Nid Oiseau', 'Mangeoire double', 'Mangeoire', 'Echelle plastique'];
        // Rongeurs
        $aKeywords[21] = ['Foin vert', 'Céréales complètes', 'Croquettes pour lapin', 'Friandises aux légumes', 'Friandise Selective Anneaux pour lapin', 'Nourriture', 'Muesli Hamsters et rongeurs'];
        $aKeywords[22] = ['Jouet', 'Accessoire', 'Super'];
        // Poissons
        $aKeywords[23] = ['Nourriture poisson', 'Nourriture poisson rouges', 'Nature pastilles à base de végétaux', 'Sachet de', 'Boîte de', 'Nourriture', 'Aliment'];
        // Reptiles
        $aKeywords[24] = ['Insectes', 'Granulés Tortues d\'eau douce', 'Nourriture en bâtonnets pour tortues ', 'Grillons séchés pour reptiles insectivores', 'Boîte de', 'Os de seiche naturel', 'Aliment']; 

        //$aKeywords = ['Shampooing', 'Gel douche', 'Friandise', 'Croquettes', 'Boîte de', 'Pâtée', 'Nourriture', 'Accessoire', 'Jouet', 'Gellules', 'Super', 'Produit anti-puce', 'Sachet de', 'Paquet de'];

        $product = new Product();
        $product->setName('Grand os végétal à mâcher');
        $product->setDescription("Os végétal pour chien à mastiquer fabriqué en France, composé de céréales et enrichi en oligomer. Convient aux petits chiens de moins de 10 kg et participe à l’amélioration de leur hygiène dentaire.");
        $details = '<ul><li>Poids d’un os : 75g</li><li>Nombre d’os par boîte : 4</li><li>Enrichi en oligomer</li></ul>';
        $product->setDetails($details);
        $product->setReference($faker->randomNumber(8, true));
        $product->setQuantityStock($faker->numberBetween(20, 150));
        $product->setPrice(8.90);
        $product->setIsActive(1);
        $product->setIsBest(1);
        $product->setBrand($this->getReference('brand'.$faker->numberBetween(1, 10)));
        $product->setCategory($this->getReference('category8'));
        $manager->persist($product);

        $photo = new Photo();
        $photo->setProduct($product);
        $photo->setUrl('produit1a.jpg');
        $photo->setPriority(10);
        $manager->persist($photo);

        $photo = new Photo();
        $photo->setProduct($product);
        $photo->setUrl('produit1b.jpg');
        $photo->setPriority(0);
        $manager->persist($photo);

        $product = new Product();
        $product->setName('Friandises à mâcher chien Better Bones');
        $product->setDescription("Les friandises à mâcher pour chiens Better Bones Wrap Twists sont élaborés avec de vrai morceaux de poulet et d’ingrédients digestes. La combinaison parfaite entre les arômes de poulet Canard / Cranberry à une texture délicieusement tendre sans cuir brut.");
        $details = '<ul><li>Torsades enrobées de poulet</li><li>Facile à digérer</li></ul>';
        $product->setDetails($details);
        $product->setReference($faker->randomNumber(8, true));
        $product->setQuantityStock($faker->numberBetween(20, 150));
        $product->setPrice(3.99);
        $product->setIsActive(1);
        $product->setIsBest(1);
        $product->setBrand($this->getReference('brand'.$faker->numberBetween(1, 10)));
        $product->setCategory($this->getReference('category8'));
        $manager->persist($product);

        $photo = new Photo();
        $photo->setProduct($product);
        $photo->setUrl('produit2a.jpg');
        $photo->setPriority(10);
        $manager->persist($photo);

        $photo = new Photo();
        $photo->setProduct($product);
        $photo->setUrl('produit2b.jpg');
        $photo->setPriority(0);
        $manager->persist($photo);

        $photo = new Photo();
        $photo->setProduct($product);
        $photo->setUrl('produit2c.jpg');
        $photo->setPriority(0);
        $manager->persist($photo);

        $product = new Product();
        $product->setName('Coussin chien en tissu déperlant rembourré');
        $product->setDescription("Coussin bien rembourré en tissu super résistant et déperlant de couleur marron, adapté aux grands chiens.");
        $details = '<ul><li>Dimensions : L120 x l80 x H13,5 cm</li><li>Couleurs : marron et orange</li></ul>';
        $product->setDetails($details);
        $product->setReference($faker->randomNumber(8, true));
        $product->setQuantityStock($faker->numberBetween(20, 150));
        $product->setPrice(45.95);
        $product->setIsActive(1);
        $product->setIsBest(1);
        $product->setBrand($this->getReference('brand'.$faker->numberBetween(1, 10)));
        $product->setCategory($this->getReference('category9'));
        $manager->persist($product);

        $photo = new Photo();
        $photo->setProduct($product);
        $photo->setUrl('produit3a.jpg');
        $photo->setPriority(10);
        $manager->persist($photo);
        
        
        for ($i=1; $i<1000; $i++) {
            $cat = $faker->numberBetween(8, $catMax);
            $product = new Product();
            $product->setName($aKeywords[$cat][random_int(0, count($aKeywords[$cat])-1)].' '.ucfirst($faker->domainWord()).' pour '.$aParentCategory[$cat]);
            $product->setDescription($faker->paragraph(2));
            $details = '<ul><li>'.$faker->sentence().'</li><li>'.$faker->sentence().'</li><li>'.$faker->sentence().'</li></ul>';
            $product->setDetails($details);
            $product->setReference($faker->randomNumber(8, true));
            $product->setQuantityStock($faker->numberBetween(20, 150));
            $product->setPrice($faker->randomFloat(1, 2, 51));
            $product->setIsActive(1);
            if($i%50 == 0) $product->setIsBest(1);
            else $product->setIsBest(0);
            $product->setBrand($this->getReference('brand'.$faker->numberBetween(1, 10)));
            $product->setCategory($this->getReference('category'.$cat));
            $manager->persist($product);

            for($j=1;$j<=random_int(1, 4); $j++){
                $photo = new Photo();
                $photo->setProduct($product);
                $photo->setUrl(random_int(1, 8).'.jpg');
                $photo->setPriority($j);
                $manager->persist($photo);
            }
        }

        $manager->flush();
    }

    public function getDependencies(){
        return [BrandFixtures::class, CategoryFixtures::class];
    }
}
