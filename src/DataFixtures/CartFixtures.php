<?php

namespace App\DataFixtures;

use App\Entity\Cart;
use App\Entity\User;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

class CartFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }
    
    public function load(ObjectManager $manager): void
    {
        $userRows = $this->em->createQuery('SELECT COUNT(u.id) FROM App\Entity\User u')->getSingleScalarResult();
        $productRows = $this->em->createQuery('SELECT COUNT(p.id) FROM App\Entity\Product p')->getSingleScalarResult();

        for ($i=1;$i<=$userRows-1;$i++) {
            if($i%5==0){
                $user = $this->em->getRepository(User::class)->find($i);
                for ($j=0;$j<=random_int(1,6);$j++) {
                    $randIdProduct = random_int(1, $productRows);
                    $product = $this->em->getRepository(Product::class)->find($randIdProduct);
                    $cart = new Cart();
                    $cart->setUser($user);
                    $cart->setProduct($product);
                    $cart->setQuantity(random_int(1, 4));
                    $manager->merge($cart);
                }
            }
        }
        $manager->flush();
    }

    public function getDependencies(){
        return [ProductFixtures::class, UserFixtures::class];
    }
}
