<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture{

    public function load(ObjectManager $manager): void
    {
        $category1 = new Category();$category1->setParent(NULL);$category1->setName('dog');$category1->setIsActive(1);$category1->setPriority(10);$manager->persist($category1);$this->addReference('category1', $category1);
        $category2 = new Category();$category2->setParent(NULL);$category2->setName('cat');$category2->setIsActive(1);$category2->setPriority(9);$manager->persist($category2);$this->addReference('category2', $category2);
        $category3 = new Category();$category3->setParent(NULL);$category3->setName('hen');$category3->setIsActive(1);$category3->setPriority(8);$manager->persist($category3);$this->addReference('category3', $category3);
        $category4 = new Category();$category4->setParent(NULL);$category4->setName('bird');$category4->setIsActive(1);$category4->setPriority(7);$manager->persist($category4);$this->addReference('category4', $category4);
        $category5 = new Category();$category5->setParent(NULL);$category5->setName('rodent');$category5->setIsActive(1);$category5->setPriority(6);$manager->persist($category5);$this->addReference('category5', $category5);
        $category6 = new Category();$category6->setParent(NULL);$category6->setName('fish');$category6->setIsActive(1);$category6->setPriority(5);$manager->persist($category6);$this->addReference('category6', $category6);
        $category7 = new Category();$category7->setParent(NULL);$category7->setName('reptile');$category7->setIsActive(1);$category7->setPriority(4);$manager->persist($category7);$this->addReference('category7', $category7);
        
        $category8 = new Category();$category8->setParent($category1);$category8->setName('dog_food');$category8->setIsActive(1);$category8->setPriority(0);$manager->persist($category8);$this->addReference('category8', $category8);
        $category9 = new Category();$category9->setParent($category1);$category9->setName('dog_accessories');$category9->setIsActive(1);$category9->setPriority(0);$manager->persist($category9);$this->addReference('category9', $category9);
        $category10 = new Category();$category10->setParent($category1);$category10->setName('dog_health');$category10->setIsActive(1);$category10->setPriority(0);$manager->persist($category10);$this->addReference('category10', $category10);
        $category11 = new Category();$category11->setParent($category1);$category11->setName('dog_care');$category11->setIsActive(1);$category11->setPriority(0);$manager->persist($category11);$this->addReference('category11', $category11);

        $category12 = new Category();$category12->setParent($category2);$category12->setName('cat_food');$category12->setIsActive(1);$category12->setPriority(0);$manager->persist($category12);$this->addReference('category12', $category12);
        $category13 = new Category();$category13->setParent($category2);$category13->setName('cat_accessories');$category13->setIsActive(1);$category13->setPriority(0);$manager->persist($category13);$this->addReference('category13', $category13);
        $category14 = new Category();$category14->setParent($category2);$category14->setName('cat_health');$category14->setIsActive(1);$category14->setPriority(0);$manager->persist($category14);$this->addReference('category14', $category14);
        $category15 = new Category();$category15->setParent($category2);$category15->setName('cat_care');$category15->setIsActive(1);$category15->setPriority(0);$manager->persist($category15);$this->addReference('category15', $category15);

        $category16 = new Category();$category16->setParent($category3);$category16->setName('hen_food');$category16->setIsActive(1);$category16->setPriority(0);$manager->persist($category16);$this->addReference('category16', $category16);
        $category17 = new Category();$category17->setParent($category3);$category17->setName('hen_accessories');$category17->setIsActive(1);$category17->setPriority(0);$manager->persist($category17);$this->addReference('category17', $category17);
        $category18 = new Category();$category18->setParent($category3);$category18->setName('hen_health&care');$category18->setIsActive(1);$category18->setPriority(0);$manager->persist($category18);$this->addReference('category18', $category18);

        $category19 = new Category();$category19->setParent($category4);$category19->setName('bird_food');$category19->setIsActive(1);$category19->setPriority(0);$manager->persist($category19);$this->addReference('category19', $category19);
        $category20 = new Category();$category20->setParent($category4);$category20->setName('bird_accessories');$category20->setIsActive(1);$category20->setPriority(0);$manager->persist($category20);$this->addReference('category20', $category20);

        $category21 = new Category();$category21->setParent($category5);$category21->setName('rodent_food');$category21->setIsActive(1);$category21->setPriority(0);$manager->persist($category21);$this->addReference('category21', $category21);
        $category22 = new Category();$category22->setParent($category5);$category22->setName('rodent_accessories');$category22->setIsActive(1);$category22->setPriority(0);$manager->persist($category22);$this->addReference('category22', $category22);

        $category23 = new Category();$category23->setParent($category6);$category23->setName('fish_food');$category23->setIsActive(1);$category23->setPriority(0);$manager->persist($category23);$this->addReference('category23', $category23);

        $category24 = new Category();$category24->setParent($category7);$category24->setName('reptile_food');$category24->setIsActive(1);$category24->setPriority(0);$manager->persist($category24);$this->addReference('category24', $category24);

        //$category25 = new Category();$category25->setParent($category8);$category25->setName('dog_food_croquette');$category25->setIsActive(1);$category25->setPriority(0);$manager->persist($category25);$this->addReference('category25', $category25);
        $manager->flush();
    }

}