<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('lucie@free.fr');
        $user->setRoles(["ROLE_ADMIN","ROLE_DEV"]);
        $user->setPassword($this->hasher->hashPassword($user, '123'));
        $user->setLastname('Bidouille');
        $user->setFirstname('Lucie');
        $user->setBirthdate(new DateTime());
        $user->setInscription(new DateTime());
        $user->setLastConnection(new DateTime());
        $manager->persist($user);

        $user = new User();
        $user->setEmail('lanimesalerie30@yopmail.com');
        $user->setRoles(["ROLE_ADMIN"]);
        $user->setPassword($this->hasher->hashPassword($user, '123'));
        $user->setLastname('Capard');
        $user->setFirstname('Dominique');
        $user->setBirthdate(new DateTime());
        $user->setInscription(new DateTime());
        $user->setLastConnection(new DateTime());
        $manager->persist($user);

        $user = new User();
        $user->setEmail('lucie.bidouille@yopmail.com');
        $user->setPassword($this->hasher->hashPassword($user, '123'));
        $user->setLastname('Bidouille');
        $user->setFirstname('Lucie');
        $user->setBirthdate(new DateTime());
        $user->setInscription(new DateTime());
        $user->setLastConnection(new DateTime());
        $manager->persist($user);

        $address = new Address();
        $address->setName('Maison');
        $address->setLastname('Bidouille');
        $address->setFirstname('Lucie');
        $address->setUser($user);
        $address->setStreetLine1('42 square grimmaurd');
        $address->setStreetLine2(null);
        $address->setZipCode('63000');
        $address->setCity('Clermont-ferrand');
        $address->setCountry('FR');
        $address->setPhone('0612354585');
        $address->setIsBilling(true);
        $address->setIsDelivery(true);

        $manager->persist($address);

        $faker = Faker\Factory::create('fr_FR');

        $password = $this->hasher->hashPassword($user, 'password');

        for($i=0;$i<=100;$i++){
            $lastname = $faker->lastName();
            $firstname = $faker->firstName();
            
            $user = new User();
            $user->setEmail(strtolower($firstname.'.'.$lastname).'@'.$faker->freeEmailDomain());
            $user->setPassword($password);
            $user->setLastname($lastname);
            $user->setFirstname($firstname);
            $user->setBirthdate($faker->dateTimeBetween('-60 year', '-20 year'));
            $user->setInscription($faker->dateTimeBetween('-1 year', '-1 week'));
            $user->setLastConnection($faker->dateTimeBetween('-1 week', 'now'));
            $manager->persist($user);

            $zip = $faker->postcode();
            $city = $faker->city();

            $address = new Address();
            $address->setName('Maison');
            $address->setLastname($lastname );
            $address->setFirstname($firstname);
            $address->setUser($user);
            $address->setStreetLine1($faker->streetAddress());
            if ($i%2 == 0) {
                $address->setStreetLine2($faker->secondaryAddress());
            } else {
                $address->setStreetLine2(null);
            }
            $address->setZipCode($zip);
            $address->setCity($city);
            $address->setCountry('FR');
            $address->setPhone($faker->phoneNumber());
            $address->setIsBilling(true);
            
            if($i%3 == 0){
                $address->setIsDelivery(false);
                $address2 = new Address();
                $address2->setName('Travail');
                $address2->setLastname($lastname );
                $address2->setFirstname($firstname);
                $address2->setUser($user);
                $address2->setStreetLine1($faker->company());
                $address2->setStreetLine2($faker->streetAddress());
                $address2->setZipCode($zip);
                $address2->setCity($city);
                $address2->setCountry('FR');
                $address2->setPhone($faker->phoneNumber());
                $address2->setIsBilling(false);
                $address2->setIsDelivery(true);
                $manager->persist($address2);
            } else {
                $address->setIsDelivery(true);
            }

            if($i%5==0){
                $firstname = $faker->firstName();
                $address3 = new Address();
                $address3->setName('Chez '.$firstname);
                $address3->setLastname($faker->lastName());
                $address3->setFirstname($firstname);
                $address3->setUser($user);
                $address3->setStreetLine1($faker->streetAddress());
                $address3->setStreetLine2(null);
                $address3->setZipCode($zip);
                $address3->setCity($city);
                $address3->setCountry('FR');
                $address3->setPhone($faker->phoneNumber());
                $address3->setIsBilling(false);
                $address3->setIsDelivery(false);
                $manager->persist($address3);
            }

            $manager->persist($address);

        }


        $manager->flush();

        dd('ici');
    }
}
