<?php

namespace App\DataFixtures;

use App\Entity\Settings;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SettingsFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $setting = new Settings();
        $setting->setName('address_line1');
        $setting->setContent('42 rue de Verdun');
        $manager->persist($setting);

        $setting = new Settings();
        $setting->setName('address_line2');
        $setting->setContent('30000 - Nîmes');
        $manager->persist($setting);

        $setting = new Settings();
        $setting->setName('address_line3');
        $setting->setContent('04 66 36 XX XX');
        $manager->persist($setting);

        $setting = new Settings();
        $setting->setName('address_line4');
        $manager->persist($setting);


        $setting = new Settings();
        $setting->setName('home_message');
        $manager->persist($setting);

        $manager->flush();
    }
}
