<?php

namespace App\DataFixtures;

use App\Entity\Carrier;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CarrierFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $carrier = new Carrier();
        $carrier->setName('Colissimo');
        $carrier->setDescription('Livraison suivie à domicile en 48h, sans signature.');
        $carrier->setPrice(4.90);
        $manager->persist($carrier);

        $carrier = new Carrier();
        $carrier->setName('Chronopost Express');
        $carrier->setDescription('Le service assure en France la livraison en 1-2 jours ouvrés suivant l’expédition.');
        $carrier->setPrice(7.90);
        $manager->persist($carrier);

        $carrier = new Carrier();
        $carrier->setName('Click & Collect');
        $carrier->setDescription('Vous pourrez retirer votre colis dans les 10 jours suivant sa disponibilité en boutique.');
        $carrier->setPrice(0);
        $manager->persist($carrier);

        $manager->flush();
    }
}
