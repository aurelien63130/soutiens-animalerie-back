<?php

namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\User;
use App\Entity\Product;
use App\Entity\OrderDetail;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Faker;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }

    public function load(ObjectManager $manager): void
    {

        $faker = Faker\Factory::create('fr_FR');

        $userRows = $this->em->createQuery('SELECT COUNT(u.id) FROM App\Entity\User u')->getSingleScalarResult();
        $productRows = $this->em->createQuery('SELECT COUNT(p.id) FROM App\Entity\Product p')->getSingleScalarResult();
        
        for ($i=0;$i<=200;$i++) {

            $order = new Order();

            $randIdUser = random_int(5, $userRows);

            $user = $this->em->getRepository(User::class)->find($randIdUser);
            
            $order->setUser($user);
            $date = $faker->dateTimeBetween('-1 year', '-1 week');
            $order->setCreatedAt($date);
            $order->setCarrierName('Colissimo');
            $order->setCarrierPrice(4.90);
            $addresses = $user->getAddresses()->getValues();
            shuffle($addresses);
            $address = $addresses[0];
            $address = str_replace('[strong]', '', $address);
            $address = str_replace('[/strong]', '', $address);
            $address = str_replace('[br]', '<br/>', $address);
            $order->setDeliveryAddress($address);
            $order->setStatus(14);
            $order->setIsPaid(true);
            $order->setReference(strtoupper(uniqid('CMD')));
            $order->setStripeSessionId(strtoupper(uniqid('fixtures_')));
            $order->setPaidAt($date);

            $manager->persist($order);

            $nbProducts = random_int(1, 5);

            for ($j=0;$j<=$nbProducts;$j++) {
                $orderDetail = new OrderDetail();
                $randIdProduct = random_int(1, $productRows);
                $product = $this->em->getRepository(Product::class)->find($randIdProduct);
                //$orderDetail->setProduct($product);
                $orderDetail->setProductName($product->getName());
                $orderDetail->setProductReference($product->getReference());
                $orderDetail->setPrice($product->getPrice());
                $orderDetail->setQuantity(random_int(1, 3));
                $order->addOrderDetail($orderDetail);
                $manager->persist($orderDetail);
            }
        }

        $manager->flush();
    }

    public function getDependencies(){
        return [ProductFixtures::class, UserFixtures::class, CarrierFixtures::class];
    }
}
