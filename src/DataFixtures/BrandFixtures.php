<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BrandFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $i=1;

        $brand = new Brand();
        $brand->setName('Tro Plan');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Royal Félin');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Felix Felicis');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Pet Avenger');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('My Pet Lover');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Chachébon');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Animaster');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Déli\'Pet');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Lily\'s Kitchen');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $i++;

        $brand = new Brand();
        $brand->setName('Kong King');
        $manager->persist($brand);
        $this->addReference('brand'.$i, $brand);

        $manager->flush();

        dd('ici');
    }
}
